﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using VegaTodo.Context;
using VegaTodo.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VegaTodo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : Controller
    {
        private readonly TodoContext _context;

        public TodoController(TodoContext context)
        {
            _context = context;
            if(_context.Todos.Count() == 0)
            {
                _context.Todos.Add(new Models.Todo { Title = "Have a nice day! (Mandatory)" });
                _context.SaveChanges();
            }
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<List<Todo>> Get()
        {
            return _context.Todos.ToList();
        }

        [HttpGet("{date}")]
        public ActionResult<List<Todo>> GetByDate(string date)
        {
            List<Todo> lista = new List<Todo>();
            foreach(Todo t in _context.Todos.ToList())
            {
                if (date.Equals(t.CurrentDate.ToString("dd-MM-yyyy")))
                {
                    lista.Add(t);
                }
            }
            return lista;
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name ="GetTodo")]
        public ActionResult<Todo> GetById(int id)
        {
            var item = _context.Todos.Find(id);
            if(item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPost]
        public ActionResult<Todo> Create([FromBody]Todo todo)
        {
            _context.Todos.Add(todo);
            _context.SaveChanges();

            return Ok(todo);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]Todo todo)
        {
            var item = _context.Todos.Find(id);
            if(todo == null)
            {
                return NotFound();
            }
            if (todo.Title == "")
            {
                item.Title = todo.Title;
            }
            
            item.isComplete = todo.isComplete;

            _context.Todos.Update(item);
            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public ActionResult<List<Todo>> Delete(int id)
        {
            var todo = _context.Todos.Find(id);
            if(todo == null)
            {
                return NotFound();
            }

            _context.Todos.Remove(todo);
            _context.SaveChanges();
            return Ok(_context.Todos.ToList());
        }
    }
}
