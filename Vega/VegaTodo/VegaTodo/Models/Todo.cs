﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VegaTodo.Models
{
    public class Todo
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public bool isComplete { get; set; }
        private DateTime currentDate = DateTime.Now;

        public DateTime CurrentDate
        {
            get { return currentDate; }
            set {
                DateTime dateNow = DateTime.Now; 
                currentDate = dateNow; }
        }

    }
}
