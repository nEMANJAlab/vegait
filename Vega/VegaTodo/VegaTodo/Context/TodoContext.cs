﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VegaTodo.Models;

namespace VegaTodo.Context
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options):
            base(options)
        { }

        public DbSet<Todo> Todos { get; set; }
    }
}
