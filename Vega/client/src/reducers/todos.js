import { FETCH_TODOS, FETCH_TODOS_SUCCESS, FETCH_TODO, FETCH_TODO_SUCCESS, CREATE_TODO, CREATE_TODO_SUCCESS, DELETE_TODO, DELETE_TODO_SUCCESS, CREATE_TODO_FAILURE } from "../actions/todos";

export default (state = {list: [], created: false, loading: false, error: false}, action) => {
    switch (action.type) {
        case DELETE_TODO:
            return { ...state }
        case DELETE_TODO_SUCCESS:
            return{ ...state,
                list: action.payload.data
        }
        case CREATE_TODO:
            return { ...state }
        case CREATE_TODO_SUCCESS:
            return{ ...state,
                list: [...state.list, action.payload.data],
                created: true
        }
        case CREATE_TODO_FAILURE:
        return {
            ...state,
            created: false,
            error: true,
            loading: false
        }
        case FETCH_TODO:
            return{ ...state}
        case FETCH_TODO_SUCCESS:
            return{
                ...state,
                todo: action.payload.data
        }
        case FETCH_TODOS:
            return{ ...state }
        case FETCH_TODOS_SUCCESS:
            return {
                ...state,
                list: action.payload.data
            }
        default:
            return state;
    }
};
