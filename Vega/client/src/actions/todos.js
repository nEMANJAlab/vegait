export const FETCH_TODOS = "todos/FETCH_TODOS";
export const FETCH_TODOS_SUCCESS = "todos/FETCH_TODOS_SUCCESS";
export const FETCH_TODOS_FAILURE = "todos/FETCH_TODOS_FAILURE";
export const FETCH_TODO = "todos/FETCH_TODO";
export const FETCH_TODO_SUCCESS = "todos/FETCH_TODO_SUCCESS";
export const FETCH_TODO_FAILURE = "todos/FETCH_TODO_FAILURE";
export const CREATE_TODO = "todos/CREATE_TODO";
export const CREATE_TODO_SUCCESS = "todos/CREATE_TODO_SUCCESS";
export const CREATE_TODO_FAILURE = "todos/CREATE_TODO_FAILURE";
export const DELETE_TODO = "todos/DELETE_TODO";
export const DELETE_TODO_SUCCESS = "todos/DELETE_TODO_SUCCESS";
export const DELETE_TODO_FAILURE = "todos/DELETE_TODO_FAILURE";

export const fetchTodos = date => ({
    type: FETCH_TODOS,
    payload: {date}
});

export const fetchTodo = id => ({
    type: FETCH_TODO,
    payload: {id}
});

export const createTodo = (todo, successCallback) => ({
    type: CREATE_TODO,
    payload: {todo},
    successCallback
});

export const deleteTodo = id => ({
    type: DELETE_TODO,
    payload: {id}
});