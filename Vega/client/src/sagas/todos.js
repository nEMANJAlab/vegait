import { call, put, takeEvery, takeLatest } from "redux-saga/effects";

import {
    fetchTodos as ft
 } from "../api/todos";

import {fetchTodo as fct} from "../api/todos";
import {createTodo as creTodo} from "../api/todos";
import {deleteTodo as delTodo} from "../api/todos";

import {
    FETCH_TODOS,
    FETCH_TODOS_SUCCESS,
    FETCH_TODOS_FAILURE,
    FETCH_TODO,
    FETCH_TODO_SUCCESS,
    FETCH_TODO_FAILURE,
    CREATE_TODO,
    CREATE_TODO_SUCCESS,
    CREATE_TODO_FAILURE,
    DELETE_TODO,
    DELETE_TODO_SUCCESS,
    DELETE_TODO_FAILURE
} from "../actions/todos";

function* fetchTodos(action) {
    try {
        const data = yield call(ft, action.payload.date);
        yield put({ type: FETCH_TODOS_SUCCESS, payload: data });
    } catch(e) {
        yield put({ type: FETCH_TODOS_FAILURE });
    }
}

function* fetchTodo(action){
    try{
        const data = yield call(fct, action.payload.id);
        yield put({type: FETCH_TODO_SUCCESS, payload: data});
    }catch(e){
        yield put({type: FETCH_TODO_FAILURE});
    }
}

function* createTodo(action){
    try{
        const data = yield call(creTodo, action.payload);
        if (data.status === 200) {
            yield call(action.successCallback);
        }
        yield put({type: CREATE_TODO_SUCCESS, payload: data});
    }catch(e){
        yield put({type: CREATE_TODO_FAILURE});
    }
}

function* deleteTodo(action){
    try{
        const data = yield call(delTodo, action.payload.id);
        yield put({type: DELETE_TODO_SUCCESS, payload: data});
    }catch(e){
        yield put({type: DELETE_TODO_FAILURE});
    }
}

export default function* todosSaga() {
    yield takeEvery(FETCH_TODOS, fetchTodos);
    yield takeLatest(FETCH_TODO, fetchTodo);
    yield takeLatest(CREATE_TODO, createTodo);
    yield takeLatest(DELETE_TODO, deleteTodo);
}

