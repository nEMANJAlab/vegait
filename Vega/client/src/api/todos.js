import axios from "axios";

export const fetchTodos = async (date) => {
    return await axios({
        url: `https://localhost:44343/api/todo/${date}`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const fetchTodo = async (id) => {
    return await axios({
        url: `https://localhost:44343/api/todo/${id}`,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}

export const createTodo = async (body) => {
    return await axios({
        url: `https://localhost:44343/api/todo`,
        method: "POST",
        headers: { 'content-type': 'application/json' },
        data: {
            title: body.todo
          }
    })
}

export const deleteTodo = async (id) => {
    return await axios({
        url: `https://localhost:44343/api/todo/${id}`,
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
}
