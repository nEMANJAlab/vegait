import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import Todo from './Todo';
import Popup from 'reactjs-popup';
import Calendar from 'react-calendar';

import { fetchTodos, fetchTodo, createTodo, deleteTodo } from "../actions/todos";
import '../internship-source-code/css/app.css';
import quotes from '../quotes.json';
import iconCalendar from '../internship-source-code/icons/icon-calendar.svg';
import iconPlus from '../internship-source-code/icons/icon-plus.svg';
import vegaLogo from '../internship-source-code/images/vegait-logo.svg';

class Todos extends React.Component {
    constructor() {
        super();

        this.state = {
            title: '',
            open: false,
            date : new Intl.DateTimeFormat('en-GB', { year: 'numeric', month: 'numeric', day: 'numeric',
            }).format(new Date())
            .replace(new RegExp("/", 'g'), "-")
        }
    }

    componentDidMount() {
        this.props.fetchTodos(this.state.date);
        this.randomQuote();
    }

    handleChange = e => {
        this.setState({
            title: e.target.value
        });
    }

    removeTodo = id => {
        setTimeout(() => { 
            this.props.deleteTodo(id);
        }, 1000);
    }

    randomQuote(){
        var random = quotes[Math.floor(Math.random()*quotes.length)];
        this.setState({
            quote: random.text,
            author:random.by
        })
    }

    openCalendar = () => {
        this.setState({open: true });
      };
     
    closeCalendar = () => {
        this.setState({open: false });
    };

    calendarDate = date => {
       var newDate = new Intl.DateTimeFormat('en-GB', { 
        year: 'numeric', 
        month: 'numeric', 
        day: 'numeric',
      }).format(new Date(date));
        this.setState({date:newDate});
        this.props.fetchTodos(newDate.replace(new RegExp("/", 'g'), "-"));
    }

    closePopup = (title, closeModal) => {
        this.props.createTodo(title, closeModal); 
    }

    render() {
        const { title } = this.state;
        const { todos, error } = this.props;
        const { open } = this.state;
        const { date } = this.state;

        return (
            <div>
                <header className="header">
                    <div className="wrap">
                        <span className="btn-icon" >
                            <Popup trigger={<img className="icon icon-plus" src={iconPlus} alt="Add New Item" />} modal>
                            {close => (
                                <div className="modal">
                                        <h2>Create a task today:</h2>
                                        {
                                            error && <p>Something bad happened.</p>
                                        }
                                        <div className="field-wrap">
                                            <input className="field" type="text" name="titleBox" placeholder="Title.." onChange={this.handleChange} />
                                        </div>
                                        <div className="btn-wrap align-right">
                                            <button disabled={!this.state.title} className="btn" onClick={() => this.closePopup(title, close) } >Create</button>
                                        </div>
                                        
                                </div>
                            )}
                            </Popup>
                        </span>
                        <div className="header-blockquote">
                            <h1 className="header-quote">{this.state.quote}</h1>
                            <div className="header-cite">- {this.state.author}</div>
                        </div>
                    </div>
                    <div className="header-inner">
                        <div className="wrap">
                            <img className="logo" src={vegaLogo} alt="VegaIT"/>
                            <div className="date-wrap">
                                <img onClick={this.openCalendar} className="icon" src={iconCalendar} alt="Calendar"/>
                                <time>{date}</time>
                            </div>
                        </div>
                    </div>
                    { open &&
                    <Popup open={open} onClose={this.closeCalendar} close modal>
                        {close => (
                            <Calendar onClickDay={this.calendarDate}/>
                        )}
                    </Popup>
                    }
                </header>

                {todos.map((todo, id) => {
                    return (
                        <Todo key={id} todo={todo} delete={() => this.removeTodo(todo.id)}/>
                    )
                })}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    todos: state.todos.list,
    todo: state.todos.todo,
    created: state.todos.created,
    error: state.todos.error
});

const mapDispatchToProps = dispatch => bindActionCreators({ fetchTodos, fetchTodo, createTodo, deleteTodo }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Todos);