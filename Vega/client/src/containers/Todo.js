import React from 'react';

const Todo = (props) => {
    return(
            <div className="wrap">
                <div className="item-row"  >
                    <label className="check-flag">
                        <span className="check-flag-label">{props.todo.title}</span>
                        <span className="checkbox">
                            <input className="checkbox-native" onChange={props.delete} type="checkbox"/>
                            <span className="checkmark">
                                <div> 
                                    <svg viewBox="0 0 24 24">
                                        <path className="checkmark-icon" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" ></path>
                                    </svg>
                                </div>
                            </span>
                        </span>
                    </label>
                </div>
            </div>
    )
}

export default Todo;