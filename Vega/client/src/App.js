import React, { Component } from 'react';
import {Provider} from "react-redux";
import store from './store';
import './App.css';
import Todos from './containers/Todos';

class App extends Component {
  constructor() {
    super();
    this.state = {
      
    };
  }

  render() {
    return (
      <Provider store={store}>
      <div className="App">
      <Todos />
      </div>
      </Provider>
    );
  }
}

export default App;
