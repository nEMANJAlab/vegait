# VegaIt

TODO application for creating daily tasks. 
- Asp.net core web application with EF
- React with redux

To start the API, import project solution into Visual Studio
	- When the project is loaded, type 'Update-Database' command in Package Manager console, and run the project.

To start client side, type 'npm-install' in the client folder, after it's done type 'npm-start'.

-To create TODO, click on the PLUS icon and write the name of the todo in the field, then click on create button.

-To list the TODO-s by date, click on the Calendar icon and pick the date.